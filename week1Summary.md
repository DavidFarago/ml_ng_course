# Machine Learning, Andrew Ng, Stanford (Coursera), Week 1

## Introduction

### Welcome-Video

Summary:  AI ubiquitous, multiple times a day, e.g. 
* web search
* spam filtering
* face recognition
* database mining
  * from automation/web, e.g. click stream data
  * medical records,
  * genome
* implicit knowledge
  * autonomous flying/driving 
  * handwriting recognition
  * NLP, computer vision
* self-customization
* to understand human learning/brain

This course: State of the art ML and **practice**.

Complex heuristics needed &rarr; use ML.

### What is ML

Def by Arthur Samual: Give computers the ability to learn without being explicitly programmed.

Def by Tom Mitchell: Program learns if its performance P on task T improves with experience E.

Classes of ML algorithms:
* supervised learning (teach computer)
* unsupervised learning (computer learns by itself)
* reinforcement learning
* recommender systems

Practice 
* very important, as for a carpenter 
* even top silicon valley companies waste 6 months due to lack of experience\
&rarr; how to/best practices/design very important

### Supervised Learning

Def: Right answers (called labels) are given in training set.

Example: Predict housing price based on size; fit linear or quadratic function to data?\
&rarr; Regression problem: predict continuous valued output (price interpreted as continuous value)

Example: Predict whether tumor is malignant based on size. \
&rarr; Classification problem: predict discrete valued output (malign or not malign)

Example with more than one feature: 
* predict whether tumor is malignant based on size and human age; 
* further features: clump thickness, uniformity of cell size, uniformity of cell shape, ...
* support vector machines can deal with an infinite number of features, which is relevant in practice.


### Unsupervised Learning

Def: No labels given in training set, finds some structure in input data.

Clustering algorithm: partitioning in the given data.

Example:  
* cohesive news stories in google news, 1 class with all stories about BP oil spills
* clustering individuals based on their genes
* network (computing clusters, social) analysis
* market segmentation of your customers
* how galaxies are formed, via astronomical data analysis.

Cocktail party algorithm: separate out, e.g. two audio sources

Octave, Matlab: only a few lines to implement cocktail party algorithm (with linear algebra routine singularValueDecomposition)\
&rarr; $`[W,s,v] = svd((repmat(sum(x.*x,1),size(x,1),1).*x)*x')`$; \
&rarr; best practice: First prototype in Octave/Matlab. Then migrate to Python/Java/C++ (much more complicated, thus much faster to prototype with Octave)

## Model and Cost Function

### Linear regression with one variable, Model representation

Training set:
* number of examples: $`m`$
* input variable or features: $`x`$ (is column in $`\R^m`$ in case of univariate regression)
* output variable or target: $`y`$ (is column in $`\R^m`$)
* **one** training example or sample: $`(x,y)`$ (is row in $`\R^m \times \R^m`$)
* $`i`$-th training example: $`(x^{(i)},y^{(i)})`$ (is row $`i`$ in $`\R^m \times \R^m`$)

Learning algorithm: $`\R^m \times \R^m \rightarrow \R^\R; x, y \mapsto h`$,\
with $`h`$ being a hypothesis for the linear function mapping $`x^{(i)}`$ to $`y^{(i)}`$ for all $`i`$, i.e. $`h`$ predicts/estimates for each input an output.

Model representation for univariate linear regression: $`h(x) := h_\Theta(x) := \Theta_0 + \Theta_1 * x`$ 

### Cost Function

How to choose parameters $`\Theta_0, \Theta_1`$ for $`h_\Theta(x)`$ for best possible line to fit data?\
So that $`h(x)`$ is as close as possible to $`y`$ for our training examples $`(x,y)`$!

Closeness via cost function (squared error loss function) $`J(\Theta_0, \Theta_1) := 1/2 * MSE(h(x),y)`$ with $`MSE(z,y) := 1/m * \Sigma_{i=1}^m (z^{(i)} - y^{(i)})^2`$. 
$`J(\Theta_0, \Theta_1)`$ is one of the most commonly used cost functions and works well for most regression functions.

As close as possible: $`min_{\Theta_0, \Theta_1} J(\Theta_0, \Theta_1)`$.

### Cost Function - Intuition II

Project 3 dimensional plot of function $`\Theta_0, \Theta_1 \rightarrow J(\Theta_0, \Theta_1)`$ onto a contour plot (a topographical 2 dimensional map) with $`J(\Theta_0, \Theta_1)`$ as color or contour lines.

## Parameter Learning

### Gradient descent

Algorithm for computing $`min_{\Theta} J(\Theta)`$, for any differentiable function $`J`$ and any dimension $`n`$ of $`\Theta`$ (i.e. not only minimizing the cost function $`J`$ from above for linear regression).

Start with some $`\Theta`$ and a fixed learning rate $`\alpha \in \R_{>0}`$.\
repeat until convergence: $`\Theta := \Theta - \alpha \nabla_\Theta J(\Theta)`$

Gradient descent converges at some local minimum and is similar to hill climbing, but adjusts all values in $`\Theta`$ in one iteration, according to the gradient of the hill.

### Gradient descent for linear regression

Gradient for our squared error loss function $`J`$ for linear $`h`$:\
$`\nabla_\Theta J(\Theta_0, \Theta_1) = \nabla_\Theta 1/2m * \Sigma_{i=1}^m (h_\Theta(x^{(i)}) - y^{(i)})^2 =  1/2m * \Sigma_{i=1}^m \nabla_\Theta (\Theta_0 + \Theta_1 * x^{(i)} - y^{(i)})^2 = 1/m * \Sigma_{i=1}^m [ h_\Theta(x^{(i)}) - y^{(i)}, (h_\Theta(x^{(i)}) - y^{(i)}) * x^{(i)} ]`$

Our squared error loss function $`J`$ for linear $`h`$ is a convex function, so there is only a global optimum. 

Very nice video of gradient descent iteration where first mainly the intercept is adjusted, then mainly the slope.

Batch gradient descent: each step uses entire batch, i.e. all training examples $`m`$.

Gradient descent scales better for large data compared to the alternative of computing $`min_{\Theta} J(\Theta)`$ non-iteratively but numerically with the normal equation method.

### Matrices and Vectors

## Linear Algebra Review

Matrix $`A \in \R^{4 \times 2}`$ has 4 rows, 2 columns, last element is $`A_{4 2}`$.

Vector $`y \in \R^4 = \R^{4 \times 1}`$, has 4 dimensions. 
If not stated otherwise, a vector is 1-indexed, i.e. starts with $`y_1`$.
In programming rather 0-indexed, i.e. starts with $`y[0]`$. 

Matrix multiplication
* not commutative
* associative
* has identidy $`I = I_{n \times n}`$ for suitable $`n`$

Square matrix $`A \in \R^{m \times m}`$ has inverse $`A^{-1}`$ iff $`A`$ is not "too close to zero" (singular or degenerate).

Transpose matrix $`A^T`$ has elements $`A^T_{i j} = A_{j i}`$.

Matlab:
* `A = [1, 2, 3, 4; 5, 6, 7, 8]` (or without `,`; or line breaks after `;`)
* `isequal(size(A), [4, 2])`
* last element: `A(2,4) == 8`
* watch out for broadcasting (aka BSX, see [1]): `isequal(A + 2, A + 2 * ones(4,2))`, i.e. not `undef`
* `I = eye(42)`
* `inverseOfA = inv(A)`, `pseudoInverseOfA = pinv(A)` (both runtimes in $`O(n^3)`$)  
* `transposeOfA = transpose(A) = A'` 
(elementwise transpose `A.'` in case $`A \in \mathbb{C}^{m \times n}`$)

Instead of $`h_\Theta(x) = -40 + 3 * x`$ for $`x \in \{21, 14, 15, 8\}`$, use a `DesignMatrix` (aka `DataMatrix`):\
`prediction := DesignMatrix * parameters = [1, 21; 1, 14; 1, 15; 1, 8] * [-40; 3] = [23; 2; 5; -16]`, which is more readable and parallelizable (vectorization).

The same for 3 competing hypotheses with $`\Theta`$=`[-40; 3]`, $`\Iota`$=`[-20; 1]`, $`\Kappa`$=`[1; 1]`:\
`PredictionColumns := DesignMatrix * ParameterMatrix = [1, 21; 1, 14; 1, 15; 1, 8] * [-40, -20, 1; 3, 1, 1] = [23, 1, 22; 2, -6, 15; 5, -5, 1; -16, -12, 9]`, with highly optimized computation (multi-core and SIMD parallelism).

[1]: https://octave.org/doc/interpreter/Broadcasting.html