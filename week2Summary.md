# Machine Learning, Andrew Ng, Stanford (Coursera), Week 2

## Multivariate Linear Regression

### Multiple Features

Multivariate linear regression, with $`n`$ features:
* $`x^{(i)}_j`$ is feature $`j`$ in training example $`i`$
* $`h_\Theta(x) := \Theta_0 + \Sigma_{i=1}^n \Theta_i * x_i = \Sigma_{i=0}^n \Theta_i * x_i = \Theta^T * x`$ with $`x_0 := 1`$ and $`x, \Theta`$ 0-indexed $`n+1`$-dimensional vectors
* `DesignMatrix` $`X \in \R^{m \times (n+1)}`$ with $`X_{i j} = x^{(i)}_{(j-1)}`$ (matrix column $`j \mapsto`$ feature $`j-1`$)
* $`h_\Theta(X) := X * \Theta`$, the $`m`$-dimensional vector of predictions.

### Gradient Descent for Multiple Variables

Gradient for our squared error loss function $`J`$ for multivariate linear $`h`$:\
$`\nabla_\Theta J(\Theta) = \nabla_\Theta 1/2m * \Sigma_{i=1}^m (h_\Theta(x^{(i)}) - y^{(i)})^2 =  1/2m * \Sigma_{i=1}^m \nabla_\Theta ( (\Sigma_{j=0}^n \Theta_j * x^{(i)}_j) - y^{(i)})^2 = 1/m * \Sigma_{i=1}^m [(h_\Theta(x^{(i)}) - y^{(i)}) * x^{(i)}_j]_{j=0}^n`$

### Gradient Descent in Practice I - Feature Scaling

Feature Scaling: Make sure all features are on a similar scale.\
Otherwise the slopes of different dimensions (dims) are too different to pick a suitable scalar learning rate $`\alpha`$ for all dims: either the dim with large slope overshoots, or the dim with small slope descents minimally per iteration (zigzags on a long elliptic contour line).\
&rarr; scale each feature to about the same range, often to [-1,1] (but difference factor 3 is ok).\
Use $`x_i:=x_i / s_i`$ for feature $`i>0`$, with $`s_i`$ being the range ($`max - min`$) of $`x_i`$ over the training set. $`s_i`$ can alternatively be the standard deviation over the training set, often called Z-score normalization.

Mean normalization (rather "centering", since some use "normalization" for scaling): Make sure all features have approximately zero mean.\
This does not improve gradient descent, but is beneficial for neural networks.\
Use $`x_i:=x_i - \mu_i`$ for feature $`i>0`$, with $`\mu_i`$ being the average of $`x_i`$ over the training set.

Overall, use $`x_i := (x_i - \mu_i)/s_i`$.

### Gradient Descent in Practice II - Learning Rate

Debugging:
* to make sure gradient descent converges
* plot $`y=J(\Theta)`$ over $`x=gradientDescentIteration`$
* very difficult to tell in advance how many iterations are necessary until convergence
* automatic convergence tests: e.g. if $`J(\Theta)`$ decreases by less than 
threshold $`\epsilon = 10^{-3}`$ in one iteration, but choosing $`\epsilon`$ is difficult
* if $`J(\Theta)`$ does not decrease every iteration: $`\alpha`$ is too large ($`J(\Theta)`$ overshoots the optimum during gradient descent);\
if $`J(\Theta)`$ decreases too slowly: $`\alpha`$ is too small (or in some unlikely cases too large)
* advice: plot above for $`\alpha \in \{3e-4, 1e-3, 3e-3, 1e-2, 3e-2, 1e-1, 3e-1, 1, 3\}`$.

### Features and Polynomial Regression

$`h_\Theta(x) = \Sigma_{i=0}^n \Theta_i * x_i`$ is linear in terms of the 
unknown parameters $`\Theta_i`$ and thus multivariate linear regression can be applied. 
For models that are polynomials of degree $`d`$, choose $`x_i = x^i`$. 
This polynomial regression allows you to use the machinery of 
multivariate linear regression to fit complex, non-linear functions.
But all $`x_i`$ are still considered distinct independent variables in this multivariate linear regression model :-0

Other examples:
* housing: instead of feature `frontage` and feature `depth`, rather use new feature `area` := `frontage` * `depth`
* other general functions, e.g. above with $`n = 2, x_0 = 1, x_1 = x, x_2 = \sqrt{x}`$.

Many possibilities, so how to decide which feature to use? Algorithms can automatically choose the best degree $`d`$ for polynomial models.

## Computing Parameters Analytically

### Normal Equation

Normal equation solves $`min_{\Theta} J(\Theta)`$ analytically, i.e. with one computation instead of iteratively like gradient descent: solve $`\nabla_\Theta J(\Theta) = 0`$ for $`\Theta`$.

With `DesignMatrix` $`X`$ and target vector $`y`$, we have $`\nabla_\Theta J(\Theta)`$ = $`\nabla_\Theta 1/2m (X * \Theta - y)^T * (X * \Theta - y)`$ = $`1/2m \nabla_\Theta (\Theta^T * X^T * X * \Theta - 2 * \Theta^T * X^T * y + y^T * y)`$ = $`1/2m (2 * X^T * X * \Theta - 2 * X^T * y)`$. 
Thus $`0 = \nabla_\Theta J(\Theta)`$ equals $`0 = X^T * X * \Theta - X^T * y`$ equals $`X^T * X * \Theta = X^T * y`$ equals the normal equation $`\Theta = (X^T * X)^{-1} * X^T * y`$ (if $`X^T * X`$ is invertible, details see [1]). 

So when is the normal equation better for a multivariate linear regression problem compared to gradient descent? The normal equation needs no learning rate, no feature scaling, and only one iteration. But gradient descent also works for learning algorithms more sophisticated than linear regression and scales better in $`n`$, esp. above 1000 (runtime in $`O(n^2 * iterations)`$ compared to $`O(n^3)`$ for gradient descent). 

<!-- 
### Normal Equation Noninvertibility

In rare cases, $`X^T * X`$ is singular, caused by your features:
* if they are linearly dependent (i.e. redundant), e.g. size in feet$`^2`$ and size in m$`^2`$
* if there are too many, e.g. $`m \leq{} n`$ (in which case you can use regularization, see week 3)

You should remove troubling features. Using the preuso-inverse `pinv(X' * X)` instead of `inv(X' * X)` also avoids the problem.

## Octave/Matlab Tutorial

Including the commands from week 1, more details see [refcard].

### Basic operations

Octave/Matlab is more productive than Python Numpy, R, Java, C++. It is great to prototype; reimplement with one of the above languages only for large scale deployments. 

Operations:
* elementary arithmetic: `+`, `-`, `*`, `/`, `^`
* logical: `! ` (negation), `0` (false), `1` (true)
* logical comparisons: `==`, `~=` (not), `&&`, `||`, `xor( , )`
* (elementwise) comparisons: `<`, `<=`, `>`, `>=`, `!=` or `~=`, `==`
* full structure equality comparison: `isequal(X1, X2, ...)` function
* assignment to variable: `=` (makes copy)
* broadcasting (aka BSX, see [1]) of binary operators (or function arguments) differing in size: missing or 1 dimensions are bloated up by duplicating the data (are broadcasted), e.g. `isequal(A + 2, A + 2 * ones(4,2))`.

Identifiers:
* untyped variables, e.g. `a = 3`, `b = 'hi'`, `c = (3>=1)`
* built-in variables, such as
  * `ans`, last result not explicitly assigned
  * `Inf` infinity, `NaN` 
  * `pi` $`\pi`$ 
* functions, such as 
  * `isequal( , , ...)` for identity comparison 
  * `strcmp( , )` for elementwise string comparison
  * `sqrt( )`.

Probability functions:
* `rand(1,3)` with random values drawn uniformly from set $`\{0, 1\}`$
* `randn(1,3)` with random values drawn from a Gaussian distributio with mean zero and variance and standard deviation 1.
* `hist(identifier)`, `hist(identifier,bucketNumbers)` for GUI histograms.

Commands: 
* `cd`, `pwd`, `ls`
* variableName
* `PS1('>> ')`
* `#` (inline comment), `##`(block comment)
* `disp(identifier)`, `disp(sprintf('2 decimals: %0.2f', identifier))`
* `format long` displays 14 decimal places, `format short` displays the default 4 decimal places
* `,` comma chaining to chain commands, `;` to chain commands and suppresses output
* `help name`.

Datastructure literals:
* matrix or vector `[1, 2, 3, 4; 5, 6, 7, 8]` (or without `,`; or line breaks after `;`)
* range `1:0.5:3` = `[1 1.5 2 2.5 3]` with a positive increment, `1:4` = `[1 2 3 4]` with the default increment 1
* `zeros(1,3)` = `[0 0 0]`, `eye(3)` = $`I_3`$, `magic_matrix` = `magic(42)`.

### Moving Data Around

Store, load, delete:
* `save fileName variableName`, or human readable with argument `-ascii` 
* `load filename` or `load('filename')`
* `clear variableName`, `clear` clears all variables.

Query:
* `size(A, 1)` returns only size of first dimension
* `length(v)` size of the longest dimension
* `who` shows variables in current scope, `whos` with a detailed view.

Indexing (the referenced submatrices can be mutated):
* array element: `A(2,4)`
* `A(1, 2:4)` column 2 to 4 of row 1, so `2:4` gives range
* `A(2, :)` second row of `A`, so `:` means every element
* `A([1 3], :)` first and third row of `A`, so `[]` lists elements
* `A(:)` special case syntax to put all elements into one long column vector.

Concatenate matrices (they are flattened): 
* `[A, [1; 2; 3]]` horizontally  
* `[A; [1, 2, 3]]` vertically.

Style (see [2]):
* Literals: When constructing matrices, prefer comma rather than space.
* Naming: Use lowercase names (no mixed case names). Uppercase is acceptable for variable names consisting of 1-2 letters.
* Strings: Use double quotes rather than single quotes (are interpreted slightly differently)
* Use specific end-of-block statements (`endif`, `endswitch`) rather than generic `end`.
* two lines after `endfunction`: first `%!demo`block, then `%!test` block (see [3]).

### Computing on Data

Matrix operations:
* `A * B` matrix multiplication
* `inverse_A = inv(A)`, `pseudoinverse_A = pinv(A)` almost inverse, but also for noninvertible matrices (both have runtimes in $`O(n^3)`$)  
* `flipud(A)` flips matrix vertically
* `transpose(A)` or `A'` (use elementwise transpose `A.'` in case $`A \in \mathbb{C}^{m \times n}`$)
* elementwise operations denoted by `.`, e.g. `A .* B`, `A .^ 2`, `1 ./ A`, 
* elementwise operations `log(A)`, `abs(A)`, `sin(A)`, `-A`, `A < 3`, `floor(A)`, `ceil(A)`, `round(A)
* dimensionwise operations: 
  * `sum(A)`, `sum(A,1), `prod(A)` per first (non-signleton) dimension of `A` (columnwise)
  * `sum(A, dim)`, `prod(A, dim)` per dimension `dim` of `A`, e.g. `sum(A, 2)` for rowwise
* maximum:
  * elementwise operation `max(A, B)`
  * dimensionwise operation `max(A)`, `max(A, [], dim)`
  * dimensionwise including index: `[val, ind] = max(A)`, `[val, ind] = max(A, [], dim)`
  * maximum element `max(max(A))=max(A(:))` 
* find:
  * `find(A < 3)` gives vector of indices (row+col) of elements that are true
  * `[row, col] = find(A < 3)` gives vector of indices (row, col) of elements that are true.

### Plotting Data

E.g. for debugging (see section "Gradient Descent in Practice II - Learning Rate" above) and to get ideas how to improve learning algorithm. 

Plotting and labeling:
* `plot(v1,v2)`, `plot(v1,v2,'r')` for red
* `imagesc(Matrix)` visualizes `Matrix` as color grid
* `axis([0.5 1 -1 1])` adds axes, `xlabel('name')` adds label
* `legend('foo','bar')` adds legend, `title('name')` adds title.

Manage plots:
* `hold on` holds on last plot for future plots
* `print -dpng 'fileName.png'` stores in `fileName.png`
* `close` closes plot
* `clf` clears figure
* `figure(number)` puts figure `number` in focus
* `subplot(x,y,element)` divides plot into `x` by `y` grid and focuses on `element`.

-->

[1]: https://eli.thegreenplace.net/2014/derivation-of-the-normal-equation-for-linear-regression/
[2]: https://wiki.octave.org/Octave_style_guide
[3]: https://wiki.octave.org/Tests
[refcard]: http://folk.ntnu.no/joern/itgk/refcard-a4
